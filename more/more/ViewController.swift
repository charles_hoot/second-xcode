//
//  ViewController.swift
//  more
//
//  Created by iOS on 2/23/17.
//  Copyright © 2017 Charles Hoot. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var colorBTN: UIButton!
    
    @IBAction func colorAction(_ sender: Any) {
        colorBTN.backgroundColor = UIColor.blue
    }
    
    @IBAction func colorScreenAction(_ sender: UIButton) {
        self.view.backgroundColor = UIColor.cyan
    }
    
    @IBAction func resetAction(_ sender: Any) {
        colorBTN.backgroundColor = UIColor.clear
        self.view.backgroundColor = UIColor.clear
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

